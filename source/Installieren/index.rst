Installieren
============

.. toctree::
   :maxdepth: 2

   Windows.md
   Linux_server.md
   Ubuntu_Deskop.md
   Remote_Ubuntu_Rechner.md
   Ubuntu_Installieren.md
   Sphinx_Doc.md

